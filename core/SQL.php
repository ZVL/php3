<?php

namespace core;

class SQL extends Container
{

    protected $db = null;

    public function init()
    {
        $config = $this['service.provider']->getService('Config');

        $dsn = sprintf(
            '%s:host=%s;dbname=%s',
            $config->getSetting('db.driver'),
            $config->getSetting('db.host'),
            $config->getSetting('db.name')
        );

        $this->db = new \PDO($dsn, $config->getSetting('db.user'), $config->getSetting('db.pass'), [
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        ]);

        $this->db->exec("SET NAMES UTF8");
    }

    public function insert($table, $params)
    {
        $masks = [];

        foreach ($params as $k => $v) {
            $masks[] = ':' . $k;
        }
        $fields = implode(', ', array_keys($params));
        $values = implode(', ', $masks);

        $sql = "INSERT INTO $table ($fields) VALUES ($values)";
        $query = $this->db->prepare($sql);
        $query->execute($params);
        $this->checkQuery($query);

        return $this->db->lastInsertId();
    }

    public function select($sql, $params = [])
    {
        $query = $this->db->prepare($sql);
        $query->execute($params);
        $this->checkQuery($query);

        return $query->fetchAll();
    }

    public function delete($table, $filter, $params = [])
    {
        $sql = "DELETE FROM $table WHERE $filter";
        $query = $this->db->prepare($sql);
        $query->execute($params);
        $this->checkQuery($query);

        return $query->rowCount();
    }

    public function update($table, $pairs = [], $filter = null, $params = [])
    {
        $masks = [];

        foreach (array_keys($pairs) as $item) {
            $masks[] = $item . ' = :' . $item;
        }
        $fields = implode(', ', $masks);

        $sql = "UPDATE $table SET $fields WHERE $filter";
        $query = $this->db->prepare($sql);
        $query->execute(array_merge($pairs, $params));
        $this->checkQuery($query);

        return $query->rowCount();
    }

    public function checkQuery($query)
    {
        if ($query->errorCode() != \PDO::ERR_NONE) {
            throw new \Exception ($query->errorInfo()[2]);
        }
    }
}