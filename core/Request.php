<?php

namespace core;

class Request extends Container
{
	const METHOD_GET = 'GET';
	const METHOD_POST = 'POST';

	public $get;
	public $post;
	public $server;

	public function __construct()
	{
		$this->get = $_GET;
		$this->post = $_POST;
		$this->server = $_SERVER;
	}

	public function methodIs()
	{
		return $this->server['REQUEST_METHOD'];
	}

	public function getUri()
	{
		return $this->server['REQUEST_URI'];
	}
}
