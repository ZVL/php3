<?php

namespace core;

class Response
{
    private $app;
    private $tmp;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->tmp = [];
    }

    public function addTmp($template, array $args = [], $widget = null)
    {
        $widget = $widget ?? 'base';

        $this->tmp[$widget] = [
            'tmp_name' => $template,
            'args' => $args
        ];

        return $this;
    }

    public function redirect($url)
    {
        header('Location: ' . $url);
    }

    public function render($status = 200)
    {
        $base = $this->tmp['base'];
        unset($this->tmp['base']);

        $i = 1;
        foreach ($this->tmp as $value) {
            $key = "widget$i";
            $base['args'][$key] = $this->buildTemplate($value['tmp_name'], $value['args']);
            $i++;
        }

        $tmp = $this->buildTemplate($base['tmp_name'], $base['args']);

        $this->setResponseStatus($status);
        echo $tmp;
    }

    public function setResponseStatus($status = 200)
    {
        http_response_code($status);
    }

    private function buildTemplate($template, $args)
    {
        extract($args);
        ob_start();

        $include = sprintf(
            '%s%s.php',
            $this->app['Config']->getSetting('base.tmp_path'),
            $template
        );

        include_once $include;

        return ob_get_clean();
    }
}