<?php

namespace core\interfaces;

/**
 * Используется для ответа сервера
 *
 * Interface ResponseInterface
 * @package core\interfaces
 */
interface ResponseInterface
{
    /**
     * Рендер шаблона из файла
     *
     * @param string $template - имя шаблона
     * @param array $args - набор переменных шаблона
     * @param integer $status - статус ответа
     */
    public function render($template, array $args, $status = 200);

    /**
     * Ответ сервера в формате json
     *
     * @param array $array - массив для ответа
     * @param int $status - статус ответа
     * @return string json
     */
    public function json(array $array, $status = 200);

    /**
     * Редирект пользователя на указанный url
     *
     * @param $url
     */
    public function redirect($url);
}