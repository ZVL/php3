<?php

$settings = include_once __DIR__ . '/../settings.php';

spl_autoload_register(function ($classname) use ($settings) {
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $settings['base']['path'] . $classname) . '.php';

    try {
        if (!file_exists($path)) {
            throw new Exception("File $path not found!", 1);
        }

        include_once $path;
    } catch (\Exception $e) {
        echo 'Error is catched!';
        echo '<br>';
        echo $e->getMessage();
        die;
    }
});

$app = new core\App(['settings' => $settings]);

session_start();

return $app;