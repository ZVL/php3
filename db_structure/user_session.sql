CREATE TABLE `user_session` (
  `id_session` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `sid` varchar(32) NOT NULL,
  `time_open` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`id_session`),
  UNIQUE KEY `user_session_sid_uindex` (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;