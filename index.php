<?php

use core\Response;
use models\User as mUser;
use models\Session as mSession;

$app = include_once 'core/bootstrap.php';

/**
 * Главная страница
 */
$app->get('/', function () use ($app) {
    (new Response($app))
        ->addTmp('base.html', ['string_var' => 'ssssss'])
        ->addTmp('some_widget.html', ['var' => 5], 'some_widget')
        ->render();
});

/**
 * Форма регистрации
 */
$app->get('/register', function () use ($app) {
    (new Response($app))
        ->addTmp('base.html')
        ->addTmp('register.html', [], 'register')
        ->render();
});

/**
 * Обработка формы регистрации
 */
$app->post('/register', function () use ($app) {
    $post = $app['service.provider']->getService('Request')->post;

    $mUser = new mUser($app['service.provider']->getService('SQL'));

    if ($mUser->register($post['login'], $post['pass'])) {
        (new Response($app))->redirect('/');
    }
});

/**
 * Форма авторизации
 */
$app->get('/login', function () use ($app) {
    (new Response($app))
        ->addTmp('base.html')
        ->addTmp('login.html', [], 'login')
        ->render();
});

/**
 * Обработка формы авторизации
 */
$app->post('/login', function () use ($app) {
    $post = $app['service.provider']->getService('Request')->post;

    $user = $app['service.provider']->getService(
        'User',
        [
            'db' => $app['service.provider']->getService('SQL'),
            'mUser' => new mUser($app['service.provider']->getService('SQL')),
            'mSession' => new mSession($app['service.provider']->getService('SQL')),
        ]
    );

    $isAuth = $user->login($post['login'], $post['pass'], $post['remember']);
    $response = new Response($app);

    if (!$isAuth) {
        $response->redirect('/login');
    } else {
        $response->redirect('/');
    }
});

/**
 * Разлогин
 */
$app->get('/logout', function () use ($app) {
    $user = $app['service.provider']->getService(
        'User',
        [
            'db' => $app['service.provider']->getService('SQL'),
            'mUser' => new mUser($app['service.provider']->getService('SQL')),
            'mSession' => new mSession($app['service.provider']->getService('SQL')),
        ]
    );

    $user->logout();
    (new Response($app))->redirect('/');

});

$app->get('/only/auth', function () use ($app) {
    $sUser = $app['service.provider']->getService(
        'User',
        [
            'db' => $app['service.provider']->getService('SQL'),
            'mUser' => new mUser($app['service.provider']->getService('SQL')),
            'mSession' => new mSession($app['service.provider']->getService('SQL')),
        ]
    );

    $user = $sUser->get();

    $response = new Response($app);

    if ($user === null) {
        $response->redirect('/login');
    }

    echo 'Private page!';
});

$app->run();




























