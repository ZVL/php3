<?php
return [
	'db' => [
		'name' => 'name',
		'user' => 'user',
		'pass' => 'pass',
		'host' => 'localhost',
		'driver' => 'mysql'
	],
	'base' => [
		'path' => __DIR__ . DIRECTORY_SEPARATOR ,
		'tmp_path'=>__DIR__ .DIRECTORY_SEPARATOR .'templates'.DIRECTORY_SEPARATOR
	]
];