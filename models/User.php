<?php

namespace models;

use core\SQL;

class User extends Base
{
    public function __construct(SQL $db)
    {
        parent::__construct($db);
        $this->table = 'user';
        $this->prim_key = 'id_user';
    }

    public function register($login, $pass)
    {
        return $this->add([
            'login' => $login,
            'pass' => $this->getHash($pass)
        ]);
    }

    public function getHash($pass)
    {
        return md5($pass);
    }
}